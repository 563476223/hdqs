
module.exports = {
    formatDate :function (date,fmt) { //author: meizz 
        var o = {
            "M+": date.getMonth() + 1, //月份 
            "d+": date.getDate(), //日 
            "h+": date.getHours(), //小时 
            "m+": date.getMinutes(), //分 
            "s+": date.getSeconds(), //秒 
            "q+": Math.floor((date.getMonth() + 3) / 3), //季度 
            "S": date.getMilliseconds() //毫秒 
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    },

    getClassName(_class){
        return target.name.substring(target.name.indexOf('<')+1,target.name.indexOf('>'));
    },

    addSingleButton(node,type){
        var c = node.getComponent(type);
        if(!c){
            c = node.addComponent(type);
            c.transition = cc.Button.Transition.SCALE
        }
        return c;
    },

    /*
    * target 方法的执行有者(一般是this)
    * btnNode 按钮的node，或者是button
    * func 为回调方法 string类型
    * tag 标识符 string类型
    */
    addButtonHandler(target,btnNode,func,tag){
        if(!target){
            console.warn('没有节点怎么添加触摸')
            return;
        }
        var name = target.name.substring(target.name.indexOf('<')+1,target.name.indexOf('>'));
        var handler = new cc.Component.EventHandler();
        handler.target = target;
        handler.component = name;
        handler.handler = func;
        handler.customEventData = tag;
        var button = btnNode instanceof cc.Button ? btnNode :  this.addSingleButton(btnNode,cc.Button);
        button.clickEvents.push(handler)
        return button
    },
    //按长度分隔字符串
    subSectionString(text,cutlen){
        var id = 0;
        var str = '' 
        var len = 0;
        for (var i=0; i<text.length; i++) {    
            var c = text.charCodeAt(i);
            if (c>127 || c==94) {    
                len += 2;    
            } else {    
                len ++;    
            }
            if(len>cutlen) {
                str += text.substring(id,i)+'\n';
                len = 0;
                id = i;
            }
        } 
        str += text.substring(id,text.length);
        return str;
    },
    arrayFindObject(arr,obj){
        for(var i=0; i<arr.length; i++){
            var a = arr[i];
            var b = true
            for(var k in obj){
                if(obj[k] != a[k]){
                    b = false;
                    break;
                }
            }
            if(b){
                return i;
            }
        }
        return -1
    },
    arrayMatchObject(arr,obj){
        for(var i=0; i<arr.length; i++){
            var a = arr[i];
            var b = true
            for(var k in obj){
                if(obj[k] != a[k]){
                    b = false;
                    break;
                }
            }
            if(b){
                return a;
            }
        }
        return null
    },
    isNumber(v){
        var b = parseInt(v)
        return b === 0 || b;
    }
}