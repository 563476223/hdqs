//自定义事件流
const Events = require('events');
const eventEmiter = new Events();
let isStop = false; //是否停止事件流
let cacheEvents = [];
module.exports = {
    on: eventEmiter.on,
    off: eventEmiter.removeAllListeners,
    emit: function () {
        if (!isStop) {
            eventEmiter.emit.apply(this, arguments);
        } else {
            cacheEvents.push(arguments);
        }
    },
    stop: function () {
        isStop = true;
    },
    run: function () {
        isStop = false;
        let self = this;
        cacheEvents.forEach(function (item) {
            eventEmiter.emit.apply(self, item);
        });
        cacheEvents = [];
    },
    clear: function () {
        cacheEvents = [];
    }
};