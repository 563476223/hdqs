
cc.Class({
    extends: require('wy-Component'),

    properties: {
        mask: cc.Node,
        content: cc.Node,
        board: cc.Node,
        closeBtn: cc.Node,
        titleText: cc.Label,
        _options: { default: {}, }
    },

    onLoad: function () {
        this._options = {
            x: 0, y: 0,
            width: 600,
            height: 500,
            isClose: true,
            isModel: true,
            isModelClose: true,
            titleText: '提示',
            content: 'Default is TextFont!!!',//理论上支持文本和node，不接受其它参数。
        };
        this.mask.on('touchend', this.tapMask, this);
        this.closeBtn.on('touchend', this.closeFun, this);
    },

    start: function () {

    },

    //参数参考onLiad中的this._options
    setOptions: function (data) {
        Object.assign(this._options, data);
        this.closeBtn.active = this._options.isClose;
        this.mask.active = this._options.isModel;
        this._canMask = this._options.isModelClose;
        this.titleText.string = this._options.titleText;
        this.node.setPosition(this._options.x, this._options.y);
        this.board.setContentSize(this._options.width, this._options.height);
        if (!!this.content) {
            let isStr = typeof this._options.content == 'string';
            if (isStr) {
                var chil = this.content.children[0];
                if (!!chil && !!chil.getComponent(cc.Label)) {
                    chil.getComponent(cc.Label).string = this._options.content;
                }
            } else {
                this.content.removeAllChildren();
                if(!!this._options.content.parent)
                    this._options.content.parent.removeChild(this._options.content);
                this.content.addChild(this._options.content);
            }
        }
    },

    tapMask: function () {
        if (this._canMask) {
            this.closeFun()
        }
    },

    closeFun: function () {
        this.node.destroy();
    },

});
