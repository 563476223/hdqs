cc.Class({
    extends: require('wy-Component'),

    properties: {
        bg: cc.Node,
        bgBorder: cc.Node,
        text: cc.Label,
        _options: {
            default: {}
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        this._options = {
            width: 450,
        }
        this.setPercent(0)
    },
    setOptions: function (options) {
        Object.assign(this._options, options);
        this.bg.width = this._options.width;
        this.bgBorder.width = this._options.width;
    },
    //设置显示百分比
    setPercent: function (percent) {
        this.percent = percent
        this.bg.width = (this._options.width - 60) * percent + 60;
        //  this.bg.setContentSize((this._options.width - 60) * percent + 60,this.bg.height)
        this.text.string = `${Math.ceil(percent * 100)}%`;
        this.text.node.x = this.bg.width
    },
    getPercent(){
        return this.percent
    }
});
