const Loader = require('wy-Loader');
cc.Class({
    extends: require('wy-Component'),

    properties: {
        _options: {
            default: {}
        },
    },

    onLoad: function () {
        this._options = {
            message: '这是一个警告框',
            time: 1.5,
            y: 0,
            callback: () => { }
        }
    },

    start: function () {

    },

    setOptions: function (data) {
        let obj = Object.assign({}, this._options, data)
        let tooltip = Loader.getInstantiate('UI/UI-TooltipObj');
        this.node.addChild(tooltip);
        this.node.y = -200 + obj.y;
        tooltip.getComponent('UI-TooltipObj').setOptions(obj);
    },

    // update (dt) {},
});
