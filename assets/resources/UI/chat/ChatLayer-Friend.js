
cc.Class({
    extends: require('wy-Component'),

    properties: {
        friendInfo : cc.Prefab,
        content :cc.Node,
        scrollView:cc.ScrollView
    },
    onLoad(){
        // this.updateFriendData();
        this.node.on('onWindowFocus',this.onWindowFocus,this);
    },

    updateFriendData(){
        var self = this;
        self.friendData = []
        self.applyCount = 0;
        this.lastId = 0;
        this.httpGet('/game/chat','friends.list',{},function(err,res){
            if(err){
                return
            }
            cc.log('好友信息:',res);
            self.applyCount = res.applyCount
            for(var i=0; i<res.friends.length; i++){
                self.friendData.push(res.friends[i]);
            }
            self.updateUI();
        })
    },

    scorllToButton(){
        // this.addMessageToBottom();
        var self = this;
        this.httpGet('/game/chat','friends.list',{lastId:this.lastId},function(err,res){
            cc.log('好友信息:',res);
            for(var i=0; i<res.friends.length; i++){
                self.friendData.push(res.friends[i]);
            }
            self.updateUI();
        })
    },


    updateUI(){
        this.content.removeAllChildren();

        cc.find('head/apply/hintbg',this.node).active = this.applyCount > 0
        cc.find('head/apply/hint',this.node).getComponent(cc.Label).string = this.applyCount
        cc.find('head/number',this.node).getComponent(cc.Label).string = this.friendData.length +'/'+50 

        var givelabel = {0:'',1:'赠送',2:'已赠送'}
        var receivelabel = {0:'',1:'领取',2:'已领取'}

        var offtime = 5*60*1000;
        var nowTime = new Date().getTime()
        this.n_receiveEnd = 0;
        for(var i=0; i<this.friendData.length; i++){
            var friend = this.friendData[i];
            if(!friend.isShow){
                var item = cc.instantiate(this.friendInfo);
                item.___id = friend._id;
                this.content.addChild(item);
                item.getComponent('UserInfo').setOptions(friend);
                var t = nowTime-friend.online
                if(friend.online<1000||t < offtime){
                    cc.find('tongyong_shuru/onlinebg/online',item).getComponent(cc.Label).string = '在线'
                    cc.find('tongyong_shuru/lastTime',item).active = false;
                } else {
                    cc.find('tongyong_shuru/lastTime',item).getComponent(cc.Label).string = '离线'+Math.floor(t/600000)+'分钟';
                }
                this.n_receiveEnd += (friend.canReceive==2?1:0);
                cc.find('tongyong_shuru/give/Label',item).getComponent(cc.Label).string = givelabel[friend.canGive]+receivelabel[friend.canReceive];
                var give = cc.find('tongyong_shuru/give',item)
                global.Common.addButtonHandler(this,give,'onBtnGive',friend._id);
                global.Common.addButtonHandler(this,item,'onBtnInfo',friend._id);
                friend.isShow = true;
                this.lastId = friend._id
            }
        }
        cc.find('info/receive',this.node).getComponent(cc.Label).string = this.n_receiveEnd+'/'+100;
    },
    onBtnGive(event,id){
        cc.log('onBtnGive')
        var friend =null;
        for(var i=0; i<this.friendData.length; i++){
            if(this.friendData[i]._id == id){
                friend = this.friendData[i];
                break;
            }
        }
        var self = this;
        this.httpGet('/game/chat','friends.receive',{_ids:friend._id},function(err,res){
            cc.log('res:',res);
            friend.canGive = 2;
            for(var j=0; j<self.content.children.length; j++){
                var item = self.content.children[j];
                if(item.___id==friend._id){
                    cc.find('tongyong_shuru/give/Label',item).getComponent(cc.Label).string = (friend.canGive==1 ? '赠送':'已赠送')+(friend.canReceive == 1?'领取':'不可领');
                }
            }
        });
    },
    onBtnAllGive(){
        var ids = ''
        for(var i=0; i<this.friendData.length; i++){
            var friend = this.friendData[i]
            if(friend.canGive == 1 || friend.canReceive == 0){
                ids += friend._id + (i!=0?',':'')
            }
        }
        var self = this;
        this.httpGet('/game/chat','friends.receive',{_ids:ids},function(err,res){
            // cc.log('res:',res);
            self.updateFriendData();
        });
    },
    onBtnInfo(event,d){
        cc.log('d:',d)
        var friend =null;
        for(var i=0; i<this.friendData.length; i++){
            if(this.friendData[i]._id == d){
                friend = this.friendData[i];
                break;
            }
        }
        
        var self = this;
        var other = global.EJMgr.pushUI('UI/myInfo/OtherUserInfo');
        other.getComponent('OtherUserInfo').setOptions({
            _id:friend._id,
            onClick : function(id){
                global.EJMgr.popUI();
                if(id == 0){
                    cc.log('查看玩家建筑');
                } else if(id == 1){
                    cc.log('删除好友')
                    self.httpGet('/game/chat','friends.remove',{_id:friend._id},function(err,res){
                        if(!err){
                            for(var i=0; i<self.friendData.length; i++){
                                if(self.friendData[i]._id == friend._id){
                                    self.friendData.splice(i,1);
                                    // self.updateUI();
                                    // return;
                                    break;
                                }
                            }
                            for(var j=0; j<self.content.children.length; j++){
                                var item = self.content.children[j];
                                if(item.___id==friend._id){
                                    // item.opacity = 0;
                                    item.getComponent(cc.Button).interactable = false;
                                    item.runAction(cc.sequence(cc.delayTime(0.5),cc.callFunc(function(){
                                        this.getComponent(cc.Animation).play();
                                    }.bind(item)),cc.delayTime(1.0),cc.removeSelf()))
                                    
                                }
                            }
                        }
                    })
                } else {
                    cc.log('聊天');
                    var node = global.UIMgr.pushUI('UI/chat/PrivateChat')
                    node.getComponent('PrivateChat').setOptions({_id:friend._id})
                }
            }
        })
    },

    onBtnRecommend(){ //推荐好友
        this.httpGet('/game/chat','friends.rcmd',{},function(err,res){
            // var node = global.EJMgr.pushUI('UI/chat/RecommendFriend')
            // node.getComponent('RecommendFriend').setOptions(res);
            var node = global.EJMgr.pushUI('UI/chat/searching')
            node.getComponent('searching').setOptions(res);
        })
        
    },
    onBtnRequest(){
        this.httpGet('/game/chat','friends.applyList',{},function(err,res){
            var node = global.EJMgr.pushUI('UI/chat/RequestFriend')
            node.getComponent('RequestFriend').setOptions(res);
        })
        

        // global.EJMgr.pushUI('UI/chat/RequestFriend')
    },
    onWindowFocus(){
        cc.log('onWindowFocus')
        this.updateFriendData();
    },
    onPageHide(){
        cc.log('friend hide');
    },
    onPageShow(){
        this.updateFriendData();
        cc.log('friend show');
    }
});
