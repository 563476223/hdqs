cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.initWindowSize();
        this._super();
        
        this.cid = 0;
        this.lastChild = null;
        this.pageView = this.node.getChildByName('panel').getComponent('PackageView');
        this.b_isFirstShow = true;
    },

    start () {
        
    },

    onBtnClose(){
        global.UIMgr.popUI();
    },

    onChangePageEvent(page,viewId){
        // var panel = this.pageView.getPages()[viewId];
        // panel.emit('onWindowFocus')
    },

    onDestroy(){
        // global.Loader.releaseResources('Chat')
    },

    onEnterWindowBegin(){
        this._super();
        if(this.b_isFirstShow)
        // this.pageView.setOptions({pageId:2});
        this.b_isFirstShow = false;
        if(this.pageId){
            this.pageView.setOptions({pageId:this.pageId});
        }
    },

    setOptions(pageId){
        this.pageId = pageId
        // this.pageView.setOptions({pageId:pageId});
    },

    onEnterWindowEnd(){
        
    },

    onWindowFocus(){ //从别的界面退回来时激活主界面时，传递到子界面去
        //获取当前激活的界面
        var panel = this.pageView.getPages()[this.pageView.getCurrentPageIndex()];
         //分发激活界面的消息
        panel.emit('onWindowFocus')
    }
    // update (dt) {},
});
