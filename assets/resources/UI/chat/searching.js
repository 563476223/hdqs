
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
       head:cc.Prefab
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
    },

    start () {
        var self=this;
        this.scheduleOnce(()=>{
            global.EJMgr.popUI();
            var node = global.EJMgr.pushUI('UI/chat/RecommendFriend')
            node.getComponent('RecommendFriend').setOptions(this.res);
        },3)
        
    },

    setOptions(options){
        
        this.arr=[cc.v2(0,0)];
        for(let i=0;i<4;i++){
            let t=Math.random()*2*Math.PI;
            let x=parseInt(Math.random()*260*Math.sin(t));
            let y=parseInt(Math.random()*260*Math.cos(t));
            let flag=true;
            for(let j=0;j<this.arr.length;j++){
                if(Math.abs(x-this.arr[j].x)<90 && Math.abs(y-this.arr[j].y)<109){
                    flag=false;
                    break;
                }
            }
            if(flag){
                this.arr.push(cc.v2(x,y));
             }else{
                 i--;
             }
        }

        for(let i=0;i<options.nearUser.length;i++){
            this.scheduleOnce(()=>{
                let item=cc.instantiate(this.head);
                this.node.addChild(item);
                
                
                item.setPosition(this.arr[i+1]);
                let icon=cc.find('view/icon',item).getComponent(cc.Sprite);
                cc.loader.load({url:options.nearUser[i].headImgUrl,type:'png'},function(err,tex){
                    if(!err){
                        icon.spriteFrame = new cc.SpriteFrame(tex);
                    }
                }.bind(this))
            },0.6*i)
        }
        this.res=options;
    },
    // update (dt) {},
});
