
cc.Class({
    extends: cc.Component,

    properties: {
        icon : cc.Sprite,
        text : cc.Label,
        tag : cc.Sprite
    },

    setOptions(options){
        options.spriteFrame && this.icon &&(this.icon.spriteFrame = options.spriteFrame);
        options.text && this.text && (this.text.string = options.text);
        options.tag!=undefined &&options.tag!=null && (this.tag.node.active = options.tag);
    }
});
