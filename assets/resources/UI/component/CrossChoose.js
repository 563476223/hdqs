
cc.Class({
    extends: require('wy-Component'),

    properties: {
        text : cc.Label,
        changeEvent :[cc.Component.EventHandler]
    },

    onBtnChoose(event,data){
        this.index += parseInt(data);
        this.index = (this.index+this.options.data.length) % this.options.data.length;
        this.updateText();
    },

    getIndex(){
        return this.index;
    },

    updateText(){
        if(this.index>= this.options.data.length){
            return;
        }
        var d = this.options.data[this.index];
        this.text.string = d;
        this.text.node.stopAllActions();
        this.text.node.setScale(0.9)
        this.text.node.runAction(cc.sequence(cc.scaleTo(0.1,1.2),cc.scaleTo(0.2,1.0)))
        
        cc.Component.EventHandler.emitEvents(this.changeEvent, this);
    },

    setOptions(options){
        this.options = options;
        this.index = this.options.default ? this.options.default : 0;
        this.updateText();
    }
});
