cc.Class({
    extends: cc.Component,

    properties: {
        format : "{0}"
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.text = this.node.getComponent(cc.Label);
        this.start = 0;
        // this.setText(100);
        this.runing = false
        // this.setText(1,100)
        this.max = 10000;
        // this.printT();
    },

    printT(){
        // var v = 1.1;
        for(var i=0; i<45; i++){
            // var v = cc.lerp(0,80,i/100);
            cc.log('i:'+i,Math.sin(Math.PI/180*i));
        }
    },

    setText(a,b){
        var start,end;
        if(b){
            start = a;
            end = b;
        } else {
            start = this.runing ? this.end : this.start;
            end = a;
        }
        
        this.start = start;
        this.end = end;
        if(this.end - this.start < 5){
            this.text.string = this.format.format(this.end);
            return;
        }
        this.runing = true;
        this.time = 0;
        this.delta = 0;
        this.delayTime = 1.5;
    },

    update (dt) {
        if(!this.runing){
            return;
        }
        if(this.time > 0){
            this.time -= dt;
            return;
        }
        
        var v = Math.floor(this.delta/this.delayTime * (this.end-this.start) + this.start);
        this.delta += dt;
        
        if(this.delta >= this.delayTime){
            this.runing = false;
            this.start = this.end;
            this.text.string = this.format.format(this.end);
        } else if(this.delta >= this.delayTime-0.15){
            this.time = Math.pow((0.15+this.delta-this.delayTime)*10,3)*0.03;
            // cc.log('v:'+this.time,(0.15+this.delta-this.delayTime));
            this.text.string = this.format.format(v);
        } else {
            this.text.string = this.format.format(v);
        }
    },
});
