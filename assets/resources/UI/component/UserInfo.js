
cc.Class({
    extends: require('wy-Component'),

    properties: {
        nickname : cc.Label,
        icon : cc.Sprite,
        bg_f : cc.Node,
        bg_m : cc.Node,
        level : cc.Label,
        sex : require('UI-Toggle'),
        idLabel : cc.Label,
        address : cc.Label
    },
    setOptions(options){
        this.options = options
        options.nickname && this.nickname && (this.nickname.string = options.nickname);
        options.id && this.idLabel && (this.idLabel.string = options.id);
        options.civilizationLevel != null && this.level && (this.level.string = options.civilizationLevel);
        options.address && this.address && (this.address.string = options.address);
        options.sex && this.sex && (this.sex.setToggle(options.sex=='1' ? 1 : 0));
        if(this.bg_m!=null && this.bg_f!=null){
            if(this.options.sex=="1"){
                this.bg_m.active=true;
            }else{
                this.bg_f.active=true;
            }
        }
        if(options.headImgUrl && this.icon){
            cc.loader.load({url:options.headImgUrl,type:'png'},function(err,tex){
                if(!err){
                    this.icon.spriteFrame = new cc.SpriteFrame(tex);
                }
            }.bind(this))
        }
    },
    onBtnShowInfo(){
        var _id = this.options._id
        var other = global.EJMgr.pushUI('UI/myInfo/OtherUserInfo');
        var self = this;
        other.getComponent('OtherUserInfo').setOptions({
            _id:this.options._id,
            onClick : function(id){
                /*global.EJMgr.popUI();
                if(id == 0){
                    cc.log('查看玩家建筑');
                } else if(id == 1){
                    cc.log('删除好友')
                    self.httpGet('/game/chat','friends.remove',{_id:friend._id},function(err,res){
                        if(!err){
                            for(var i=0; i<self.friendData.length; i++){
                                if(self.friendData[i]._id == friend._id){
                                    self.friendData.splice(i,1);
                                    // self.updateUI();
                                    // return;
                                    break;
                                }
                            }
                            for(var j=0; j<self.content.children.length; j++){
                                var item = self.content.children[j];
                                if(item.___id==friend._id){
                                    // item.opacity = 0;
                                    item.getComponent(cc.Button).interactable = false;
                                    item.runAction(cc.sequence(cc.delayTime(0.5),cc.callFunc(function(){
                                        this.getComponent(cc.Animation).play();
                                    }.bind(item)),cc.delayTime(1.0),cc.removeSelf()))
                                    
                                }
                            }
                        }
                    })
                } else {
                    cc.log('聊天');
                    global.UIMgr.loadLayer('Chat','UI/chat/PrivateChat',{_id:_id});
                    // var node = global.UIMgr.pushUI('UI/chat/PrivateChat')
                    // node.getComponent('PrivateChat').setOptions({_id:friend._id})
                }*/
            }
        })
    }
});
