
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        friendItem : cc.Prefab,
        content :cc.Node
    },

    onLoad(){
        this._super();
        this.extraRes = {301:0,302:0,303:0,304:0,305:0,306:0};
    },

    updateUI(){
        cc.find('views/empty',this.node).active = this.giveData.length == 0
        this.content.removeAllChildren();
        if(this.giveData.length > 0 ){
            for(var i=0; i<this.giveData.length; i++){
                var giveInfo = this.giveData[i]
                var node = cc.instantiate(this.friendItem)
                node.__id = giveInfo._id;
                this.content.addChild(node);
                node.js.setOptions(giveInfo.user);
                var props = node.getChildByName('prop');
                for(var j=0; j<4; j++){
                    var prop = props.getChildByName('item'+j);
                    
                    if(j<giveInfo.gives.length){
                        prop.getComponent('Prop').setId(giveInfo.gives[j].id,giveInfo.gives[j].count)
                    } else {
                        prop.active =false
                    }
                }
                global.Common.addButtonHandler(this,node.getChildByName('get'),'onBtnGive',giveInfo._id);
            }
        }
    },
    onBtnGive(event,id){
        var self = this;

        this.httpGet('/game/gameUser',this.options.extra+'.receiveGives',{_id:id},function(err,res){
            var ix = global.Common.arrayFindObject(self.giveData,{_id:id})
            var giveInfo = self.giveData[ix]
            self.giveData.splice(ix,1);
            var chd = global.Common.arrayMatchObject(self.content.children,{__id:id});
            chd.destroy();
            
            for(var j=0; j<res.gives.length; j++){
                var d = res.gives[j];
                self.extraRes[d.id] += d.count;
            }
        })
    },

    removeFromUI(e,d){
        this._super(e,d);
        this.options.cb && this.options.cb(this.extraRes);
    },

    setOptions(options){
        this.options = options;
        this.giveData = options.gives;
        this.updateUI();
    }
});
