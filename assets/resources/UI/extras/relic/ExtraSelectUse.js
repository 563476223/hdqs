
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        btn: [cc.Button]
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    onBtnSelect(e,d){
        this.selectCB&& this.selectCB(d);
    },

    setOptions(unuse,p,cb){
        if(unuse){
            this.btn[0].interactable = false;
        }
        this.selectCB = cb
        var p1 = this.node.convertToNodeSpace(p);
        cc.log('p:',p,p1)
        
        this.node.getChildByName('views').setPosition(p1)
    }

    // update (dt) {},
});
