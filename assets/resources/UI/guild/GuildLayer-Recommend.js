
cc.Class({
    extends: require('wy-Component'),

    properties: {
        content : cc.Node,
        guildItem : cc.Prefab
    },

    onLoad(){
        var self = this;
        this.o_async = new CCAsync();
        this.o_async.parallel([function(cb){
            self.scheduleOnce(function(){
                cb(null,1)
            },0.5)
        },function(cb){
            self.httpGet('/game/gameUser','sociality.getRecommendGuilds',{}, cb);    
        }],function(err,result){
            delete self.o_async
            self.o_async = null;
            if(err){
                global.UI.ToolTip({message:'加载失败'+JSON.stringify(err)});
                return;
            }
            self.updateGuilds(result[1])
        })
    },
    onDestroy(){
        if(this.o_async){
            delete self.o_async
        }
    },

    updateGuilds(guilds){
        this.o_guilds = guilds
        this.content.removeAllChildren();
        for(var i=0; i<guilds.length; i++){
            var guild = guilds[i]
            var item = cc.instantiate(this.guildItem);
            this.content.addChild(item);
            item.x = -500
            item.runAction(cc.sequence(cc.delayTime(0.15*(i+1)),cc.moveTo(0.15,0,0)))
            cc.find('desc/name',item).getComponent(cc.Label).string = guild.name;
            cc.find('desc/id',item).getComponent(cc.Label).string = guild.id;
            cc.find('num/bg/num',item).getComponent(cc.Label).string = guild.userCount+'/'+20;
            global.Common.addButtonHandler(this,item.getChildByName('join'),'onBtnJoin',guild._id);
        }
    },
    onBtnJoin(event,id){
        cc.log('onBtnJoin:',id)
        this.httpGet('/game/guild','applyJoin',{guildId:id}, function(err,res){
            cc.log('申请结果:',res);
            if(res){
                global.UI.ToolTip({message:'加入成功'})
            } else {
                global.UI.ToolTip({message:'申请成功'})
            }
        }); 
    }
});
