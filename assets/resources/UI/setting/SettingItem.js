
cc.Class({
    extends: cc.Component,

    properties: {
        icon: cc.Sprite,
        label:cc.Label,
        btn:cc.Node,
        voice:cc.Prefab,
        logout:cc.Prefab,
        protocol:cc.Prefab,
        clear:cc.Prefab,
        service:cc.Prefab,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.arrPrefab=[this.voice,this.logout,this.protocol,this.clear,this.service];
        this.arrName=['声音开关','注销登录','用户协议','清理资源','联系客服']
    },

    start () {

    },

    setOptions(data){
        this.label.string=this.arrName[data];
        let item = cc.instantiate(this.arrPrefab[data]);
        this.btn.addChild(item);
    }
    // update (dt) {},
});
