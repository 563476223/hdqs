//本类只做场景管理用，不作业务逻辑
var WxFileRead = require('WxFileRead')
cc.Class({
    extends: require('wy-Component'),

    properties: {
    },

    onLoad(){
        this.scene = 0;
        this.clearScene();

        this.no_bgImage = cc.find('Canvas/bg');
        this.no_mask = cc.find('Canvas/Mask');
        this.no_progress = cc.find('Canvas/Loadding')
        this.b_isRuning = false;

        var self = this;
        cc.game.on(cc.game.EVENT_SHOW,function(){
            if(self.b_isRuning){//CC_WECHATGAME
                console.log('launchoption:',global.Wechat.getLaunchOption())
                self.no_progress.active = true;
                self.clearScene();

                global.Wechat.getLocation((data)=>{

            
                    self.httpGet('/game/gameuser','getMessage',{latitude:data.latitude,longitude:data.longitude}, function(err,res){
                        self.no_progress.active = false;
                        if(err){
                            UI.Alert({title:'提示',message:'缓存有错，需要重新登陆',okButtonCallback:function(){
                                global.Wechat.cleanResource()
                            }})
                            return
                        }
                        Object.assign(global.Data,res);
                        console.log('登陆数据:',global.Data);
                        var url = global.Data.configUrl+'?a='+new Date().getTime()
                        WxFileRead.getConfigFile('config.json',global.Config.httpRootUrl+url,global.Data.configVersion,function(err,res){
                            console.log('配置数据:',global.Data);
                            if(err){
                                global.UI.Alert({title:'提示',message:'未获取到配置文件，需要重新登陆',okButtonCallback:function(){
                                    global.Wechat.cleanResource()
                                }})
                            } else {
                                global.DataConfig = res.data.gameConfig
                                self.startLoop();
                            }
                        })
                        
                    });

                })
                
            }
        })
    },

    clearScene(){
        global.UIMgr.clearNode();
        global.EJMgr.clearNode();
        this.no_headPanel = null;
        this.no_readyScene = null
        this.no_infoButton = null;
        this.no_menuBtns = null;
        this.no_MutualLayer = null;
        this.no_chatScroll = null;
    },

    //启动或重启 场景管理
    startLoop(){
        this.b_isRuning = true;
        this.node.removeAllChildren();

        this.no_buildScene = global.Loader.getInstantiate('prefab/build/BuildScene')
        this.node.addChild(this.no_buildScene);

        this.no_readyScene = global.Loader.getInstantiate('prefab/gameReady/readyPanel')
        this.node.addChild(this.no_readyScene);
        this.no_readyScene.emit('onEnterWindowBegin')

        this.setPanelActive('no_infoButton',true,'prefab/lobby/InfoButton')
        this.setPanelActive('no_headPanel',true,'prefab/lobby/HeadPanel')
        this.setPanelActive('no_menuBtns',true,'prefab/lobby/MenuBtns') 
        this.setPanelActive('no_chatScroll',true,'prefab/lobby/ChatScrollLayer')

        if(CC_WECHATGAME){
            wx.postMessage({
                messageType : 1,
                openType : wx.RankKey,
                score : global.Data.store.civilizationLevel
            })
        }
    },
    //两边附加菜单
    getMenuBtnsLayer(){
        return this.no_menuBtns
    },
    //顶部信息
    getHeadPanelLayer(){
        return this.no_headPanel
    },
    //抽奖界面
    getReadyLayer(){
        return this.no_readyScene;
    },
    //开启后将挡住一切触摸 一般用于切换动态
    setMaskShow(show){
        cc.log('setMaskShow:',show)
        this.no_mask.active = show;
    },
    updateHeadResources(){
        this.no_headPanel.js.updateResource();
    },
    
    setPanelActive(params,active,url){
        if(!this[params] && active){
            this[params] = global.UIMgr.addUI(url)
            this[params].emit('onEnterWindowBegin')
        } else if(this[params] && !active){
            this.destoryNode(params)
            this[params] = null;
        }
        return null
    },

    destoryNode(params,cb){
        var node = this[params]
        if(node){
            node.emit('onExitWindowBegin');
            this.node.runAction(cc.sequence(cc.delayTime(0.5),cc.callFunc(function(){
                node.emit('onExitWindowEnd');
                node.destroy();
                cb&&cb()
            })))
        }
    },

    toBuildLayer(){ //建造界面
        this.destoryNode('no_menuBtns')
        this.setPanelActive('no_menuBtns',false)
        this.no_readyScene.js.onExitMainScene();
        this.no_buildScene.js.onEnterMainScene();
    },
    toReadyLayer(){//抽奖界面
        this.no_buildScene.js.onExitMainScene();
        this.no_readyScene.js.onEnterMainScene();

        this.setPanelActive('no_headPanel',true,'prefab/lobby/HeadPanel')
        this.setPanelActive('no_menuBtns',true,'prefab/lobby/MenuBtns')
    },

    toMutualLayer(action){//跳转到 交互界面 包括 偷取和捣乱界面
        this.destoryNode('no_menuBtns')
        this.setPanelActive('no_menuBtns',false)
        this.setPanelActive('no_infoButton',false);
        this.setPanelActive('no_headPanel',false);
        this.setPanelActive('no_chatScroll',false);
        var ismutual = this.no_MutualLayer

        this.no_MutualLayer = global.Loader.getInstantiate('prefab/mutual/'+action)
        this.node.addChild(this.no_MutualLayer);
        this.no_MutualLayer.x = 720;
        // this.no_BrokeLayer.emit('onEnterWindowBegin')

        if(!ismutual){ //重复切换捣乱或偷取场景时
            var self = this
            for(var i=0; i<this.node.children.length; i++){
                var nd = this.node.children[i];
                nd.runAction(cc.sequence(cc.moveBy(0.8,cc.v2(-720,0)),cc.callFunc(function(no){
                    if(self.no_MutualLayer != no)
                        no.destroy();
                })))
            }
        } else {
            ismutual.runAction(cc.sequence(cc.moveBy(0.8,cc.v2(-720,0)),cc.callFunc(function(no){
                no.destroy();
            })))
            this.no_MutualLayer.runAction(cc.moveBy(0.8,cc.v2(-720,0)));
        }
        
        this.no_bgImage.runAction(cc.moveTo(0.8,cc.v2(-720,0)));
        this.no_readyScene = null;
        this.no_buildScene = null;
        return this.no_MutualLayer;
    },
    mutualToMainLayer(){ //跳转到主界面，需要提前判断
        if(global.a_mutual.length > 0){ //如果还有剩下的捣乱或偷取的话，则先用完
            this.executeMutual();
            return;
        }
        if(this.no_MutualLayer){
            var self = this
            this.no_MutualLayer.runAction(cc.sequence(cc.moveBy(0.8,cc.v2(720,0)),cc.callFunc(function(no){
                self.no_MutualLayer.destroy();
                self.node.removeAllChildren();
                self.no_MutualLayer = null;
                self.startLoop();
            })))
            this.no_bgImage.runAction(cc.moveBy(0.8,cc.v2(720,0)));
        }
    },

    executeMutual(){ //如果有捣乱或偷取就跳转界面
        if(global.a_mutual.length == 0){
            return;
        }
        var n = global.a_mutual.shift();
        if(n == 6) {
            this.httpGet('/game/gameuser','island.ship.troublemaker',{}, function(err,res){
                if(!err){
                    global.GameLoop.toMutualLayer('Broke').getComponent('Broke').setOptions(res);
                }
            })
        } else {
            this.httpGet('/game/gameuser','island.vegetableField.stealer',{}, function(err,res){
                if(!err){
                    global.GameLoop.toMutualLayer('Steal').getComponent('Steal').setOptions(res);
                }
            })
        }
    },


});
