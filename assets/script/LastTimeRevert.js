
cc.Class({
    extends: cc.Component,

    properties: {
        label : cc.Label,
        format : '{0}:{1}:{2}'
    },

    start(){
        this.b_hasHour = this.format.split(':').length == 3;
    },

    setTime(t,cb){
        this.endTime = t;
        this.endCB = cb;
        this.runTime = 1;
        this.runing = true;
    },

    update(dt){
        if(!this.runing || !this.endTime){
            return
        }
        this.runTime += dt;
        if(this.runTime >= 1){
            this.runTime = 0;
            var t = new Date().getTime();
            if(t > this.endTime){
                this.label.string = this.format.format(0,0,0)
                this.endCB && this.endCB();
                this.runing = false
                return;
            }
            var d = (this.endTime - t)/1000;
            var hour = Math.floor(d / (60*60));
            var min = Math.floor((d-hour*3600)/60);
            var sec = Math.floor(d % 60);
            if(this.b_hasHour)
                this.label.string = this.format.format(hour,min,sec)
            else
                this.label.string = this.format.format(min,sec)
        }
    }
});
