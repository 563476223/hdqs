var SoundMgr = {
    init:function(){
        var music = cc.sys.localStorage.getItem('musicOpen');
        var sound = cc.sys.localStorage.getItem('soundOpen');
        var musicVolume = cc.sys.localStorage.getItem('musicVolume');
        var soundVolume = cc.sys.localStorage.getItem('soundVolume');
        this.musicOpen = !!music ? (music==1?true:false) : true;
        this.soundOpen = !!sound ? (sound==1?true:false) : true;
        this.musicVolume = !!musicVolume?musicVolume : 1.0;
        this.soundVolume = !!soundVolume?soundVolume : 1.0;
        if(typeof this.musicVolume != 'number'){
            this.musicVolume = 1.0
        }
        if(typeof this.soundVolume != 'number'){
            this.soundVolume = 1.0
        }

        this.lastMusic = null;
        this._arrResSound = {};
        this._arrResMusic = {};
        this.lastMusic = null
        this.loadChip = ''
        this.relativePath = ''
    },

    
    getMusicVoume(){
        return this.musicVolume
    },
    setMusicVoume(r){
        this.musicVolume = r;
        cc.audioEngine.setVolume(this.lastMusic,r);
        cc.sys.localStorage.setItem('muiscVolume',r)
    },
    getSoundVoume(){
        return this.soundVolume
    },
    setSoundVoume(r){
        this.soundVolume = r;
        // cc.audioEngine.setVolume(r);
        cc.sys.localStorage.setItem('soundVolume',r)
    },
    setMusicOpen(r){
        cc.log('setMusicOpen:',r)
        if(this.musicOpen != r){
            this.musicOpen = r;
            cc.sys.localStorage.setItem('musicOpen',r?1:0)
            !r ? this.stopMusic(): this.playMusic(this.loadChip);
        }
    },
    getMusicOpen(){
        return this.musicOpen;
    },
    setSoundOpen(r){
        this.soundOpen = r;
        cc.log('setSoundOpen:',r)
        cc.sys.localStorage.setItem('soundOpen',r?1:0)
    },
    getSoundOpen(){
        return this.soundOpen;
    },

    playMusic:function(ichip,noRelative){
        var chip = (noRelative?ichip:this.relativePath+ichip)
        cc.log('playMusic chip:',chip)
        this.loadChip = ichip;
        this.lastChip = chip
        if(this._arrResMusic[chip]){
            this.stopMusic()
            this.playBackground(this._arrResMusic[chip])
            return;
        }

        if(typeof(chip) == "string"){
            var self = this
            cc.loader.loadRes(chip,function(cp,err,res){
                cc.log('playMusic:',this.lastMusic,cp)
                if(cp == this.lastChip){
                    this.stopMusic()
                    this.playBackground(res)
                }
                this._arrResMusic[cp] = res;
            }.bind(this,chip))
        } else {
            cc.log('只支持path类')
        }
    },

    playSound:function(ichip,noRelative){
        var chip = (noRelative?ichip:this.relativePath+ichip)
        cc.log('playMusic:',chip,typeof(chip))
        if(this._arrResSound[chip]){
            this.playEffect(this._arrResSound[chip])
            return;
        }
        if(typeof(chip) == "string"){
            var self = this
            cc.loader.loadRes(chip,function(cp,err,res){
                this.playEffect(res)
                this._arrResSound[cp] = res;
            }.bind(this,chip))
        } else {
            cc.log('只支持path类')
        }
    },

    playBackground(chip){
        if(this.musicOpen){
            this.lastMusic = cc.audioEngine.play(chip,-1,1);
            cc.audioEngine.setLoop(this.lastMusic, true);
        }
    },

    playEffect(chip){
        if(this.musicOpen){
            var aid = cc.audioEngine.playEffect(chip,false);
            cc.audioEngine.setVolume(aid,this.soundVolume);
        }
    },

    playButtonEffect(){
        SoundMgr.playSound('resources/game/sound/button_select.mp3',true)
    },

    setRelativePath(path){
        this.relativePath = path;
    },

    stopMusic(){
        if(this.lastMusic != null){
            cc.audioEngine.stop(this.lastMusic);
        }
        this.lastMusic = null;
    },
    releaseMusicCache:function(){
        this.release(this._arrResMusic);
    },
    releaseSoundCache:function(){
        this.release(this._arrResMusic);
    },
    release:function(arr){
        for(var k in arr){
            var res = arr[k]
            cc.loader.releaseRes(res);
        }
    }
}
module.exports = SoundMgr